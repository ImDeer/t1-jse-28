package t1.dkhrunina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-yaml-faster";

    @NotNull
    private static final String DESCRIPTION = "Load data from FasterXML YAML file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[Load data from FasterXML YAML]");
        final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @Nullable final String yaml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}