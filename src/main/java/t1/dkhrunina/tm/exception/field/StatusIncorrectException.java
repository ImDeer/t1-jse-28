package t1.dkhrunina.tm.exception.field;

public final class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error: status is incorrect");
    }

}